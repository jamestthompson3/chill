FROM node:12 AS builder
RUN mkdir -p /opt/build;
WORKDIR /opt/build
COPY . .
RUN yarn install --no-progress
RUN yarn build


FROM nginx:latest
RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /opt/build/build /opt/app/build
