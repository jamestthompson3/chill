import { Machine, assign } from "xstate";
import { client } from "../data/GraphQLClient";
import { gql } from "@apollo/client";

import { NUM_ROUTES } from "../constants";

export const initialContext = {
  routes: [],
  error: undefined,
};

const getQuery = ({ to, from }) => `
{
  plan(from: {lat: ${from.lat}, lon: ${from.lon}}, to: {lat: ${to.lat}, lon: ${to.lon}}, numItineraries: ${NUM_ROUTES}) {
    itineraries {
      walkDistance
      duration
      legs {
        mode
        startTime
        endTime
        from {
          lat
          lon
          name
          stop {
            code
            name
          }
        }
        to {
          lat
          lon
          name
        }
        agency {
          gtfsId
          name
        }
        distance
        legGeometry {
          length
          points
        }
      }
    }
  }
}
`;

export const leavenow = Machine(
  {
    id: "leave-now",
    strict: true,
    initial: "IDLE",
    context: initialContext,
    states: {
      IDLE: {
        on: {
          FETCH: "PENDING",
        },
      },
      PENDING: {
        invoke: {
          src: "getIternaries",
          onDone: "SUCCESS",
          onError: "ERROR",
        },
      },
      SUCCESS: {
        entry: "saveRoutes",
        on: {
          FETCH: "PENDING",
        },
      },
      ERROR: {
        entry: "saveError",
      },
    },
  },
  {
    services: {
      getIternaries: (_, { data: { to, from } }) => {
        return client
          .query({
            query: gql`
              ${getQuery({ to, from })}
            `,
          })
          .then(
            ({
              data: {
                plan: { itineraries },
              },
            }) => itineraries
          );
      },
    },
    actions: {
      saveRoutes: assign({
        routes: (_, e) => e.data,
      }),
      saveError: assign({
        error: (_, e) => e.data.toString(),
      }),
    },
  }
);
