import React from "react";
import { Button, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { LeaveNow } from "./pages/LeaveNow/index";
import { DESTINATIONS } from "./constants";

function getDestinationImg(destination) {
  if (destination === DESTINATIONS.HOLIDAY) {
    return (
      <span role="img" aria-label="holiday">
        🌴
      </span>
    );
  }
  return (
    <span role="img" aria-label="work">
      💼
    </span>
  );
}

const useStyles = makeStyles((theme) => ({
  appContainer: {
    height: "100%",
    width: "100%",
    overflowX: "hidden",
  },
  button: {
    [theme.breakpoints.down("md")]: {
      marginTop: 15,
      width: "100%",
    },
  },
  headerContainer: {
    display: "flex",
    flexWrap: "wrap",
    padding: "2rem 4rem",
    alignItems: "center",
    justifyContent: "space-between",
  },
}));

function App() {
  const classes = useStyles();
  const [[to, from], setDestination] = React.useState([
    DESTINATIONS.HOLIDAY,
    DESTINATIONS.WORK,
  ]);
  return (
    <div className={classes.appContainer}>
      <header className={classes.headerContainer}>
        <Typography variant="h1" style={{ textAlign: "center" }}>
          {getDestinationImg(to)} {to}
        </Typography>
        <Button
          variant="outlined"
          className={classes.button}
          onClick={() => setDestination([from, to])}
        >
          {getDestinationImg(from)}
        </Button>
      </header>
      <LeaveNow destinations={[to, from]} />
    </div>
  );
}

export default App;
