import React from "react";
import { render } from "@testing-library/react";
import App from "./App";

test("renders learn react link", () => {
  const { getByText } = render(<App />);
  const titleElement = getByText(/holiday/i);
  expect(titleElement).toBeInTheDocument();
  const vibeLabel = getByText(/I want to/i);
  expect(vibeLabel).toBeInTheDocument();
});
