/* eslint-disable jsx-a11y/accessible-emoji */
import { useMachine } from "@xstate/react";
import React from "react";
import {
  Box,
  Typography,
  Select,
  FormControl,
  InputLabel,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { leavenow } from "../../machines/leave-now.machine";
import { RouteDirections } from "./RouteDirections";
import { DESTINATION_COORDS, NUM_ROUTES } from "../../constants";

function getRandomRoute() {
  return Math.floor(Math.random() * NUM_ROUTES) + 0;
}

const useRouteStyles = makeStyles((theme) => ({
  leaveContainer: {
    display: "flex",
    flexWrap: "wrap",
    padding: "0rem 4rem",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      padding: "1rem",
    },
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const usePageStyles = makeStyles((theme) => ({
  pageWrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    padding: "2rem 0rem",
    [theme.breakpoints.down("md")]: {
      height: "auto",
    },
  },
  headerBox: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
    marginBottom: 48,
    [theme.breakpoints.down("md")]: {
      marginBottom: 12,
    },
  },
  subheader: {
    marginRight: 15,
    [theme.breakpoints.down("md")]: {
      marginRight: 0,
      marginBottom: 10,
    },
  },
}));

const useFormStyles = makeStyles((theme) => ({
  formStyles: {
    "& option": {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
  },
}));

function GetVibes({ setVibe, currentVibe }) {
  const { formStyles } = useFormStyles();
  return (
    <FormControl className={formStyles} variant="outlined">
      <InputLabel htmlFor="get-vibe">Vibe</InputLabel>
      <Select
        native
        value={currentVibe}
        onChange={(e) => setVibe(e.target.value)}
        label="Vibe"
        inputProps={{
          name: "vibe",
          id: "get-vibe",
        }}
      >
        <option aria-label="None" value="" />
        <option value="walking">🚶 Get my steps</option>
        <option value="speed">🦔 Get there fast</option>
        <option value="talk">🌃 Enjoy the city</option>
        <option value="random">🤷 Be suprised</option>
        <option value="all">🤔 See all my options</option>
      </Select>
    </FormControl>
  );
}

function RenderRoutes({ currentVibe, routes, to, from }) {
  const classes = useRouteStyles();
  switch (currentVibe) {
    case "all":
      return (
        <div className={classes.leaveContainer}>
          {routes.map((route, i) => (
            <RouteDirections key={i} route={route} from={from} to={to} />
          ))}
        </div>
      );
    case "walking": {
      let route;
      routes.forEach((r) => {
        // no current longest walking route
        if (!route) {
          route = r;
        } else if (route.walkDistance < r.walkDistance) {
          route = r;
        }
      });
      return (
        <div className={classes.leaveContainer}>
          <RouteDirections route={route} from={from} to={to} />
        </div>
      );
    }
    case "speed": {
      let route;
      routes.forEach((r) => {
        // no current fastest route
        if (!route) {
          route = r;
        } else if (r.duration < route.duration) {
          route = r;
        }
      });
      return (
        <div className={classes.leaveContainer}>
          <RouteDirections route={route} from={from} to={to} />
        </div>
      );
    }
    case "talk": {
      let route;
      routes.forEach((r) => {
        // no current longest route
        if (!route) {
          route = r;
        } else if (r.duration > route.duration) {
          route = r;
        }
      });
      return (
        <div className={classes.leaveContainer}>
          <RouteDirections route={route} from={from} to={to} />
        </div>
      );
    }
    case "random": {
      const route = routes[getRandomRoute()];
      return (
        <div className={classes.leaveContainer}>
          <RouteDirections route={route} from={from} to={to} />
        </div>
      );
    }
    case "":
      return null;
    default:
      return null;
  }
}

export function LeaveNow({ destinations }) {
  const classes = usePageStyles();
  const [currentVibe, setVibe] = React.useState("");
  const to = DESTINATION_COORDS[destinations[0]];
  const from = DESTINATION_COORDS[destinations[1]];
  const [currentState, send] = useMachine(leavenow);
  React.useEffect(() => {
    send({ type: "FETCH", data: { to, from } });
    // eslint-disable-next-line
  }, [to, from]);

  const {
    context: { routes },
  } = currentState;

  return (
    <div className={classes.pageWrapper}>
      <div className={classes.headerBox}>
        <Typography variant="h4" className={classes.subheader}>
          I want to:
        </Typography>
        <GetVibes {...{ currentVibe, setVibe }} />
      </div>
      {currentVibe && currentState.value === "PENDING" && (
        <Box display="flex" justifyContent="center" alignItems="center">
          <CircularProgress />
        </Box>
      )}
      {currentState.value === "SUCCESS" && (
        <RenderRoutes
          {...{ currentVibe, routes }}
          to={destinations[0]}
          from={destinations[1]}
        />
      )}
    </div>
  );
}
