import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import { MS_MIN, MODES, DESTINATIONS } from "../../constants";

const useStyles = makeStyles((theme) => ({
  container: {
    flex: "1 1 auto",
    margin: "5px 15px",
    maxWidth: "370px",
    background: "white",
    display: "flex",
    flexDirection: "column",
    padding: "1.25rem",
    borderRadius: "3px",
    boxShadow:
      "rgba(0, 0, 0, 0.1) 0px 4px 6px -1px,rgba(0, 0, 0, 0.06) 0px 2px 4px -1px",
    "& h2": {
      textAlign: "center",
      fontWeight: "bold",
      width: "100%",
    },
    "& .light": {
      fontWeight: "lighter",
      color: "#888",
    },
    "& h3": {
      fontWeight: "normal",
    },
  },
  legRow: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "space-around",
    width: "90%",
  },
}));

function ModeIcon({ mode }) {
  switch (mode) {
    case MODES.WALK:
      return (
        <span role="img" aria-label="walk">
          🚶‍♀️
        </span>
      );
    case MODES.BUS:
      return (
        <span role="img" aria-label="bus">
          🚌
        </span>
      );
    case MODES.RAIL:
    case MODES.TRAM:
      return (
        <span role="img" aria-label="tram">
          🚋
        </span>
      );
    case MODES.SUBWAY:
      return (
        <span role="img" aria-label="metro">
          🚇
        </span>
      );
    default:
      return null;
  }
}

function getLegTime(start, end) {
  return Math.round((end - start) / MS_MIN);
}

function getFromName(name, from) {
  if (name === "Origin") {
    return from === DESTINATIONS.WORK ? "Office 🏢" : "Holiday 🌴";
  }
  return name;
}

function getToName(name, to) {
  if (name === "Destination") {
    return to === DESTINATIONS.WORK ? "Office 🏢" : "Holiday 🌴";
  }
  return name;
}

export function RouteDirections({ route, to, from }) {
  const [displayMap, setDisplayMap] = React.useState(false);
  const classes = useStyles();
  return (
    <div
      className={classes.container}
      onClick={() => setDisplayMap(!displayMap)}
    >
      <h2>{Math.round(route.duration / 60)} minutes</h2>
      {route.legs.map((leg, i) => (
        <div
          key={leg.endTime + i}
          style={{ display: "flex", justifyContent: "center" }}
        >
          <h3 className={classes.legRow}>
            <ModeIcon mode={leg.mode} />
            <span style={{ color: "#8a1dd5", fontWeight: "bold" }}>
              {getLegTime(leg.startTime, leg.endTime)} min{" "}
            </span>
            {getFromName(leg.from.name, from)} <span className="light">to</span>{" "}
            {getToName(leg.to.name, to)}
          </h3>
        </div>
      ))}
    </div>
  );
}
