export const DESTINATIONS = {
  HOLIDAY: "holiday",
  WORK: "work",
};

export const DESTINATION_COORDS = {
  work: { lat: 60.169361, lon: 24.925833 },
  holiday: { lat: 60.168748, lon: 24.959171 },
};

export const MODES = {
  WALK: "WALK",
  BUS: "BUS",
  RAIL: "RAIL",
  TRAM: "TRAM",
  SUBWAY: "SUBWAY",
};

export const MS_MIN = 60_000;
export const NUM_ROUTES = 6;
